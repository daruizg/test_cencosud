var should = require('chai').should()

var request = require('request');
var parse = require('parse-link-header');

/**
 * Pruebas Unitarias de las casas
 */
describe('Test of houses', function () {

    /**
     * Prueba de la api para una sola casa, se verifica que tenga un nombre.
     */
    it('deberia retornar una casa', function (done) {

        request("https://anapioficeandfire.com/api/houses/378",
            { timeout: 5000 }, function (error, response, body) {
                if (!error) {
                    var res = JSON.parse(body);
                    res.name.should.be.a("string");
                    done();
                } else {
                    done(error);
                }
            });
    });

    /**
     * Prueba de la api para listado de casas ( según la api son 10 si no se declara).
     */

    it('deberia retornar 10 casas casa', function (done) {

        request("https://anapioficeandfire.com/api/houses",
            { timeout: 5000 }, function (error, response, body) {
                if (!error) {
                    var res = JSON.parse(body);
                    res.should.have.lengthOf(10);
                    done();
                } else {
                    done(error);
                }
            });

    });
    /**
     * Prueba de la api para listado de 15 casas
     */
    it('deberia retornar 15 casas casa', function (done) {

        request("https://anapioficeandfire.com/api/houses?page=1&pageSize=15",
            { timeout: 5000 }, function (error, response, body) {
                if (!error) {
                    var res = JSON.parse(body);
                    res.should.have.lengthOf(15);
                    done();
                } else {
                    done(error);
                }
            });

    });


    /**
     * Prueba de los datos de paginación de la api con las casas
     */
    describe('Paginación', function () {
        it('deberia retornar pagina anterior 1 y pagina siguiente 3', function (done) {

            request("https://anapioficeandfire.com/api/houses?page=2&pageSize=15",
                { timeout: 5000 }, function (error, response, body) {
                    if (!error) {
                        let pages = parse(response.headers.link);

                        pages.next.page.should.equal('3');
                        pages.prev.page.should.equal('1');
                        done();
                    } else {
                        done(error);
                    }
                });

        });
    });
});

/**
 * Prueba de la api para una sola casa, se verifica que tenga un nombre.
 */

describe('Test of characters', function () {
    it('deberia retornar una personaje', function (done) {

        request("https://anapioficeandfire.com/api/characters/378",
            { timeout: 5000 }, function (error, response, body) {
                if (!error) {
                    var res = JSON.parse(body);
                    res.name.should.be.a("string");
                    done();
                } else {
                    done(error);
                }
            });

    });

    /**
     * Prueba de la api para listado de casas ( según la api son 10 si no se declara).
     */

    it('deberia retornar 10 personajes', function (done) {

        request("https://anapioficeandfire.com/api/houses",
            { timeout: 5000 }, function (error, response, body) {
                if (!error) {
                    var res = JSON.parse(body);
                    res.should.have.lengthOf(10);
                    done();
                } else {
                    done(error);
                }
            });

    });

    /**
     * Prueba de la api para listado de 15 personajes
     */

    it('deberia retornar 15 personajes', function (done) {

        request("https://anapioficeandfire.com/api/characters?page=1&pageSize=15",
            { timeout: 5000 }, function (error, response, body) {
                if (!error) {
                    var res = JSON.parse(body);
                    res.should.have.lengthOf(15);
                    done();
                } else {
                    done(error);
                }
            });

    });

    /**
     * Prueba de los datos de paginación de la api con los personajes
     */
    describe('Paginación', function () {
        it('deberia retornar pagina anterior 2 y pagina siguiente 4', function (done) {

            request("https://anapioficeandfire.com/api/characters?page=3&pageSize=15",
                { timeout: 5000 }, function (error, response, body) {
                    if (!error) {
                        let pages = parse(response.headers.link);

                        pages.next.page.should.equal('4');
                        pages.prev.page.should.equal('2');
                        done();
                    } else {
                        done(error);
                    }
                });

        });
    });
});