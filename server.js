const express = require('express');
const app = express();
var http = require('http').Server(app);
var parse = require('parse-link-header');
var request = require('request');

const SERVER_PORT = 3000;
const API_URL = "https://anapioficeandfire.com";


/**
 * Todo lo que tenga el contexto static será redirigido a la carpeta public
 */
app.use('/static', express.static('public'));

process.on('uncaughtException', function (err) {
  console.error(err);
  console.log("Node NOT Exiting...");
});

/**
 * Todo las url que contengan la palabra api, será redirigido a la api de GOT.
 */
app.get('/api*', function (req, res) {
try{
  request(API_URL + req.originalUrl, {timeout: 5000}, function (error, response, body) {

    let pages=parse(response.headers.link); // se parsea la paginacion
    let resp={data:JSON.parse(body),pages:pages}; //se agrega la paginación a la respuesta

    res.send(resp);
  });
}catch(e){
  res.send({error:"Ha ocurrido un error con la api"});
}

});

/**
 * Se crea el servidor node en el puerto indicado
 */
http.listen(SERVER_PORT, function () {
  console.log('listening on *:' + SERVER_PORT);
});

/**
 * Contextos para el proxy 
 */
var CNTX = {
  "static": 'http://node:3000',
  "api": ''
};
