
var checkPagination = function (page, size) {
    let query = "";
    let pageSize = 50;

    if (page) {
        pageSize = (size) ? size : pageSize;
        query = "?page=" + page + "&pageSize=" + pageSize;
    }

    return query;
}


var app = angular.module("myApp", ["ngRoute"]);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/characters", {
            templateUrl: "characters.html",
            controller: "characterController"
        })
        .when("/houses", {
            templateUrl: "houses.html",
            controller: "houseController"
        }).otherwise({
            templateUrl: "welcome.html"
        });
});

app.controller("indexController", function ($scope, $http, $location) {

    $scope.arriba = function () {

        window.scrollTo(0, 0);
    }

    $scope.goTo = function (path) {
        $location.path(path)
    };
});

app.controller("characterController", function ($scope, $http, $filter) {
    $scope.characterList = [];

    $scope.init = function () {
        $scope.loadList(1, 40);
    };

    $scope.loadList = function (page, size) {
        var query = checkPagination(page, size);
        $http({
            method: 'GET',
            url: "/api/characters" + query
        }).then(function (success) {

            if ($scope.characterList instanceof Array)
                $scope.characterList = $scope.characterList.concat(success.data.data);
            else
                $scope.characterList.push(success.data.data);


            $scope.pages = angular.copy(success.data.pages);

        }, function (error) {
            console.log(error)

        });
    }

    $scope.openModal = function (character) {
        $scope.modalChar = character;
        $(function () {
            $('#myModal').modal("show");
        });
    }

    $scope.init();
});

app.controller("houseController", function ($scope, $http, $filter) {
    $scope.houseList = [];

    $scope.init = function () {
        $scope.loadList(1, 40);
    };

    $scope.openModal = function (house) {
        $scope.modalHouse = house;
        $(function () {
            $('#myModal').modal("show");
        });
    }

    $scope.getCharacter = function (house) {
        let spl = house.currentLord.split("/");
        let character = spl[(spl.length - 1)];

        $http({
            method: 'GET',
            url: "/api/characters/" + character
        }).then(function (success) {

            house.lord = angular.copy(success.data.data.name);
            console.log($scope.houseList);

        }, function (error) {
            console.log(error)

        });
    }

    $scope.loadList = function (page, size) {
        var query = checkPagination(page, size);
        $http({
            method: 'GET',
            url: "/api/houses" + query
        }).then(function (success) {

            if ($scope.houseList instanceof Array)
                $scope.houseList = $scope.houseList.concat(success.data.data);
            else
                $scope.houseList.push(success.data.data);


            $scope.pages = angular.copy(success.data.pages);
            console.log($scope.pages);


        }, function (error) {
            console.log(error)

        });
    }

    $scope.init();

});

// $(function () {
//     $('#myModal').modal("show");
//     });
