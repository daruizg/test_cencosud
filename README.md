# Test Cencosud Daniel Ruiz

### La app funcionando la pueden probar en  [AWS](http://34.215.207.86:3000/static/)

La prueba enviada fue realizada con las siguientes tecnologías

  - Servidor nodejs
  - Docker para el ambiente

  ## Descripción de la solución

  Por falta de tiempo elegí la solución que consideré más rápida para poder entregar el ejercicio.
  
  Servidor: nodejs con basicamente dos funciones: 
  - Servir archivos estaticos.
  - Proxy de la api anapioficeandfire.com.

la configuración del servidor está en el archivo server.js de la raíz del proyecto.

  App: Fue creada con angularjs 1.6.9, la única razón para utilizar este framework es que estoy trabajando con el en mi trabajo actual y el desarrollo sería más rápido, tambien usé bootstrap 4.
  Toda la app se encuentra en la carpeta public.

  Pruebas Unitarias: Se realizaron pruebas unitarias solo a la api para comprobar el funcionamiento de la misma, únicamente se validó que estuviera respondiendo de manera adecuada para una o muchas casas y personajes, tambien se validaron las respuestas de las paginaciones.
  Las pruebas unitarias se encuentran en el archivo test.js

Con tiempo suficiente habría validado el comportamiento del front haciendo pruebas funcionales con selenium.

  ### Instalación

El ambiente se encuentra dockerizado, por lo que si tiene instalado **docker y docker-compose** le será mucho más facil probar el proyecto en local, en caso contrario puede debe [descargar](https://nodejs.org/es/download/) e instalar nodejs en su computador.

**El primer paso para correr el proyecto es clonar el [repositorio](https://nodejs.org/es/download/)**, una vez con el repositorio clonado puede seguircon las instalaciones.


### Instalación con docker-compose

Ubicarse carpeta raiz del proyecto y ejecutar el comando
**docker-compose up** y se debe esperar a que el proyecto descargue todas las dependencias por primera vez **(1 min)**

### Instalación normal

Una vez instalado nodejs en su computador debe ubicarse en la carpeta raiz del proyecto y ejecutar **npm install && npm test && npm start** se debe esperar a que el proyecto descargue todas las dependencias por primera vez **(1 min)**

Una vez ejecutado el proyecto puede probar su funcionamiento de manera local ingresando a la siguiente URL [http://localhost:3000/static/](http://localhost:3000/static/)

Si tiene alguna duda por favor comunicarse con Daniel Ruiz daruizg@gmail.com.